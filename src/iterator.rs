use crate::Node;

enum TreeSetIteratorSide {
    Left,
    Middle,
    Right,
}

pub struct TreeSetIter<'tree, T: Ord> {
    stack: Vec<(&'tree Node<T>, TreeSetIteratorSide)>,
}

impl<'tree, T: Ord> TreeSetIter<'tree, T> {
    pub(crate) fn new(root: &'tree Option<Box<Node<T>>>) -> Self {
        let stack: Vec<(&Node<T>, TreeSetIteratorSide)> =
            root.as_ref().map_or_else(Vec::new, |root| {
                vec![(root.as_ref(), TreeSetIteratorSide::Left)]
            });
        Self { stack }
    }
}

impl<'tree, T: Ord> Iterator for TreeSetIter<'tree, T> {
    type Item = &'tree T;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if self.stack.is_empty() {
                return None;
            }

            let (parent, side) = self.stack.pop().expect("stack is not empty");

            match side {
                TreeSetIteratorSide::Left => {
                    if let Some(child) = &parent.left {
                        self.stack.push((parent, TreeSetIteratorSide::Middle));
                        self.stack.push((child, TreeSetIteratorSide::Left));
                    } else {
                        self.stack.push((parent, TreeSetIteratorSide::Middle));
                    }
                }
                TreeSetIteratorSide::Middle => {
                    parent.right.as_ref().iter().for_each(|&child| {
                        self.stack.push((parent, TreeSetIteratorSide::Right));
                        self.stack.push((child, TreeSetIteratorSide::Left));
                    });
                    return Some(&parent.value);
                }
                TreeSetIteratorSide::Right => continue,
            };
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::TreeSet;

    #[test]
    fn iterator_functions() {
        let mut tree = TreeSet::new();

        (1..=100)
            .into_iter()
            .for_each(|num| assert!(tree.insert(num).is_ok()));

        let items = tree.iter().collect::<Vec<_>>();

        (1..=100)
            .into_iter()
            .zip(items.iter())
            .for_each(|(expected, &&actual)| assert_eq!(expected, actual));
    }

    #[test]
    #[ignore]
    fn large_iteration_no_overflow() {
        let mut tree = TreeSet::new();

        (1..=1_000_000)
            .into_iter()
            .for_each(|num| assert!(tree.insert(num).is_ok()));

        let items = tree.iter().collect::<Vec<_>>();

        (1..=1_000_000)
            .into_iter()
            .zip(items.iter())
            .for_each(|(expected, &&actual)| assert_eq!(expected, actual));
    }
}
