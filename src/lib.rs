#![deny(clippy::pedantic)]

use std::{
    borrow::{Borrow, BorrowMut},
    fmt::{Debug, Formatter},
};

pub use crate::node::{ElementAlreadyExists, ElementDoesNotExist};
use crate::{iterator::TreeSetIter, node::Node};

mod iterator;
mod node;

pub struct TreeSet<T: Ord> {
    len: usize,
    root: Option<Box<Node<T>>>,
}

impl<T: Ord> TreeSet<T> {
    #[must_use]
    pub fn new() -> Self {
        Self { len: 0, root: None }
    }

    #[must_use]
    pub fn from_vec(mut elements: Vec<T>) -> Self {
        let mut set = Self::new();
        elements.drain(..).for_each(|element| {
            // We can ignore the results,
            // as everything must have been valid to have already been in a set.
            let _ = set.insert(element);
        });
        set
    }

    pub fn clear(&mut self) {
        self.root = None;
        self.len = 0;
    }

    /// Adds new elements to the `TreeSet`
    /// # Errors
    /// Returns `Err(ElementAlreadyExists)` if the element already exists in the set.
    ///
    ///# Usage
    ///```
    /// # use binary_tree::TreeSet;
    /// # let mut  tree = TreeSet::new();
    /// assert!(tree.insert(1).is_ok());
    /// assert!(matches!(tree.insert(1), Err(ElementAlreadyExists)));
    /// ```
    ///
    pub fn insert(&mut self, value: T) -> Result<(), ElementAlreadyExists> {
        if let Some(node) = &mut self.root {
            node.insert(value)?;
        } else {
            self.root = Some(Box::new(Node::new(value)));
        }

        Node::rebalance_if_needed(self.root.borrow_mut());

        if let Some(root) = self.root.as_mut() {
            root.update_height();
        }

        self.len += 1;
        Ok(())
    }

    #[must_use]
    /// Returns the number of elements in the `TreeSet`.
    /// ```
    /// # use binary_tree::TreeSet;
    /// let mut tree = TreeSet::new();
    /// assert_eq!(tree.len(), 0);
    ///
    /// (1..=5).into_iter().for_each(|num| {
    ///     let _ = tree.insert(num);
    /// });
    ///
    /// assert_eq!(tree.len(), 5);
    /// ```
    pub fn len(&self) -> usize {
        self.len
    }

    #[must_use]
    /// Returns true if the set contains no elements
    /// ```
    /// # use binary_tree::TreeSet;
    /// let mut tree = TreeSet::new();
    /// assert!(tree.is_empty());
    /// tree.insert(1).unwrap();
    /// assert!(!tree.is_empty());
    /// ```
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    #[must_use]
    pub fn iter(&self) -> TreeSetIter<T> {
        TreeSetIter::new(&self.root)
    }

    /// # Errors
    /// Returns an `Err(ElementDoesNotExist)` if the element to delete does not exist
    pub fn remove(&mut self, key: &T) -> Result<(), ElementDoesNotExist> {
        if self.is_empty() {
            return Err(ElementDoesNotExist);
        }

        let root = self.root.take().expect("Cannot be None if set not empty");
        self.root = root.remove(key)?;

        Node::rebalance_if_needed(&mut self.root);
        self.len -= 1;
        Ok(())
    }

    pub fn contains(&self, element: &T) -> bool {
        self.root
            .as_ref()
            .map_or(false, |root| root.contains(element))
    }

    pub fn get<Q>(&self, value: &Q) -> Option<&T>
    where
        T: Borrow<Q> + Ord,
        Q: Ord + ?Sized,
    {
        self.root.as_ref().and_then(|root| root.get(value))
    }
}

impl<T: Ord> Default for TreeSet<T> {
    fn default() -> Self {
        TreeSet::new()
    }
}

impl<T: Clone + Ord> TreeSet<T> {
    #[must_use]
    pub fn with_slice(elements: &[T]) -> Self {
        let mut set = Self::new();
        for element in elements.iter() {
            // Explicitly drop result of add,
            // as we don't care if it is already in the set
            let _ = set.insert(element.clone());
        }
        set
    }
}

impl<T: Debug + Ord> Debug for TreeSet<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_list().entries(self.iter()).finish()
    }
}

impl<T: Clone + Ord> Clone for TreeSet<T> {
    fn clone(&self) -> Self {
        Self {
            len: self.len(),
            root: self.root.clone(),
        }
    }
}

impl<T: Ord, const N: usize> From<[T; N]> for TreeSet<T> {
    fn from(arr: [T; N]) -> Self {
        arr.into_iter().collect()
    }
}

impl<T: Ord> FromIterator<T> for TreeSet<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let mut tree = TreeSet::new();

        iter.into_iter().for_each(|element| {
            let _ = tree.insert(element);
        });

        tree
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_create_tree_set() {
        let tree = TreeSet::<i32>::new();
        assert_eq!(tree.len, 0);
        assert!(tree.root.is_none());
    }

    #[test]
    fn can_add_single_element() {
        let mut tree = TreeSet::new();

        assert_eq!(tree.len, 0);
        assert!(tree.root.is_none());

        assert!(tree.insert(1).is_ok());

        assert_eq!(tree.len, 1);
        assert!(tree.root.is_some());

        let node = tree.root.unwrap();
        assert_eq!(node.value, 1);
        assert!(node.left.is_none());
        assert!(node.right.is_none());
    }

    #[test]
    fn can_add_many_elements() {
        let mut tree = TreeSet::new();

        assert!(tree.insert(2).is_ok());
        assert_eq!(tree.len, 1);

        let root = tree.root.as_ref().unwrap();
        assert_eq!(root.value, 2);
        assert!(root.left.is_none());
        assert!(root.right.is_none());

        assert!(tree.insert(1).is_ok());
        assert_eq!(tree.len, 2);

        let root = tree.root.as_ref().unwrap();
        assert!(root.left.is_some());
        assert!(root.right.is_none());

        let left = root.left.as_ref().unwrap();
        assert_eq!(left.value, 1);
        assert!(left.left.is_none());
        assert!(left.right.is_none());

        assert!(tree.insert(3).is_ok());
        assert_eq!(tree.len, 3);

        let root = tree.root.as_ref().unwrap();
        assert!(root.left.is_some());
        assert!(root.right.is_some());

        let right = root.right.as_ref().unwrap();
        assert_eq!(right.value, 3);
        assert!(right.left.is_none());
    }

    #[test]
    fn can_add_5_elements() {
        let mut tree = TreeSet::new();
        assert!(tree.insert(1).is_ok());
        assert!(tree.insert(2).is_ok());
        assert!(tree.insert(3).is_ok());
        assert!(tree.insert(4).is_ok());
        assert!(tree.insert(5).is_ok());
        assert_eq!(tree.len(), 5);
    }

    #[test]
    fn cannot_add_existing_element() {
        let mut tree = TreeSet::new();
        assert!(tree.insert(1).is_ok());
        assert!(tree.insert(1).is_err());
        assert_eq!(tree.len, 1);

        assert!(tree.insert(100).is_ok());
        assert!(tree.insert(100).is_err());
        assert_eq!(tree.len, 2);
    }

    #[test]
    fn can_clear_a_set() {
        let mut tree = TreeSet::new();
        assert!(tree.insert(1).is_ok());
        assert!(tree.insert(2).is_ok());
        assert!(tree.insert(-5).is_ok());
        assert_eq!(tree.len, 3);

        tree.clear();
        assert_eq!(tree.len, 0);
        assert!(tree.root.is_none());
    }

    #[test]
    fn can_get_size() {
        let mut tree = TreeSet::new();
        assert_eq!(tree.len(), 0);
        assert!(tree.is_empty());

        (1..5).into_iter().enumerate().for_each(|(idx, num)| {
            assert!(tree.insert(num).is_ok());
            assert_eq!(tree.len(), idx + 1);
        });
    }

    #[test]
    fn cannot_remove_when_no_elements() {
        let mut tree = TreeSet::new();

        assert_eq!(tree.len(), 0);

        assert!(tree.remove(&1).is_err());
    }

    #[test]
    fn can_remove_single_element_tree() {
        let mut tree = TreeSet::new();

        tree.insert(1).unwrap();
        assert_eq!(tree.len(), 1);

        tree.remove(&1).unwrap();
        assert_eq!(tree.len(), 0);
        assert!(tree.is_empty());

        assert!(tree.root.is_none());
    }

    #[test]
    fn remove_fails_if_element_not_in_tree() {
        let mut tree = TreeSet::from_vec((1..=5).into_iter().collect());

        assert!(tree.remove(&7).is_err());
    }

    #[test]
    fn can_remove_somewhere_node() {
        let mut tree = TreeSet::from_vec((1..=100).into_iter().collect());

        assert!(tree.remove(&50).is_ok());
        assert_eq!(tree.len(), 99);
    }

    #[test]
    fn remove_remains_avl() {
        let mut tree = TreeSet::from_vec((1..=5).into_iter().collect());
        //     2
        //  1       4
        //       3     5

        assert!(tree.remove(&4).is_ok());
        //      2
        //  1      3
        //           5

        assert!(tree.remove(&2).is_ok());
        //      3
        //  1       5

        assert_eq!(tree.len(), 3);
        let root = tree.root.as_ref().unwrap();
        assert_eq!(root.value, 3, "root is not valid");

        let left = root.left.as_ref().unwrap();
        assert_eq!(left.value, 1, "left is not valid");

        let right = root.right.as_ref().unwrap();
        assert_eq!(right.value, 5, "right is not valid");
    }

    #[test]
    fn can_create_from_vec() {
        let tree = TreeSet::from_vec((1..25).into_iter().collect());

        let tree_iter = tree.iter();
        let real_iter = 1..25;

        tree_iter
            .zip(real_iter)
            .for_each(|(tree_val, real_val)| assert_eq!(tree_val, &real_val));
    }

    #[test]
    fn can_create_from_slice() {
        let vec: Vec<_> = (1..25).into_iter().collect();
        let tree = TreeSet::with_slice(&vec);

        let tree_iter = tree.iter();
        let real_iter = 1..25;

        tree_iter
            .zip(real_iter)
            .for_each(|(tree_val, real_val)| assert_eq!(tree_val, &real_val));
    }

    #[test]
    fn contains_is_true_for_item_in_set() {
        let tree: TreeSet<_> = (1..25).collect();

        (1..25)
            .into_iter()
            .for_each(|element| assert!(tree.contains(&element)));
    }

    #[test]
    fn contains_is_false_for_item_not_in_set() {
        let tree: TreeSet<_> = (1..25).collect();

        assert!(!tree.contains(&59));

        assert!(!tree.contains(&12569));
    }
}
