use std::{
    borrow::{Borrow, BorrowMut},
    cmp::{max, Ordering},
    fmt::{Debug, Formatter},
};

use thiserror::Error;

pub(crate) struct Node<T: Ord> {
    pub(crate) value: T,
    pub(crate) left: Option<Box<Node<T>>>,
    pub(crate) right: Option<Box<Node<T>>>,
    height: u32,
}

impl<T: Ord> Node<T> {
    pub(crate) fn new(value: T) -> Self {
        Self {
            value,
            left: None,
            right: None,
            height: 1,
        }
    }

    fn balance_factor(&self) -> i64 {
        i64::from(self.right.as_ref().map_or(0, |node| node.height))
            - i64::from(self.left.as_ref().map_or(0, |node| node.height))
    }

    pub(crate) fn insert(&mut self, value: T) -> Result<(), ElementAlreadyExists> {
        let next = match value.cmp(&self.value) {
            Ordering::Less => &mut self.left,
            Ordering::Greater => &mut self.right,
            Ordering::Equal => return Err(ElementAlreadyExists),
        };

        if let Some(next) = next {
            next.insert(value)?;
        } else {
            next.replace(Box::new(Node::new(value)));
        };

        Self::rebalance_if_needed(next);

        self.update_height();

        Ok(())
    }

    pub(crate) fn remove(mut self, key: &T) -> Result<Option<Box<Node<T>>>, ElementDoesNotExist> {
        match key.cmp(&self.value) {
            Ordering::Less => {
                if let Some(left) = self.left.take() {
                    self.left = left.remove(key)?;
                } else {
                    return Err(ElementDoesNotExist);
                }

                let mut new_node = Some(Box::new(self));
                Node::rebalance_if_needed(new_node.borrow_mut());
                Ok(new_node)
            }
            Ordering::Greater => {
                if let Some(right) = self.right.take() {
                    self.right = right.remove(key)?;
                } else {
                    return Err(ElementDoesNotExist);
                }

                let mut new_node = Some(Box::new(self));
                Node::rebalance_if_needed(new_node.borrow_mut());
                Ok(new_node)
            }
            Ordering::Equal => match (self.left.take(), self.right.take()) {
                (None, None) => Ok(None),
                (Some(left), None) => Ok(Some(left)),
                (None, Some(right)) => Ok(Some(right)),
                (Some(mut left), Some(right)) => {
                    let node = if let Some(mut rightmost) = left.rightmost_child() {
                        rightmost.left = Some(left);
                        rightmost.right = Some(right);

                        rightmost
                    } else {
                        left.right = Some(right);
                        left
                    };

                    let mut new_node = Some(node);
                    new_node.as_mut().iter_mut().for_each(|node| {
                        node.update_height();
                    });

                    Node::rebalance_if_needed(new_node.borrow_mut());
                    Ok(new_node)
                }
            },
        }
    }

    fn rightmost_child(&mut self) -> Option<Box<Node<T>>> {
        match self.right {
            Some(ref mut right) => {
                if let Some(t) = right.rightmost_child() {
                    Some(t)
                } else {
                    let mut r = self.right.take();
                    if let Some(ref mut r) = r {
                        self.right = std::mem::replace(&mut r.left, None);
                    }
                    r
                }
            }
            None => None,
        }
    }

    pub(crate) fn rebalance_if_needed(node: &mut Option<Box<Node<T>>>) {
        if let Some(rebalancing_node) = node.take() {
            let balanced_node = if rebalancing_node.needs_rebalance() {
                rebalancing_node.rebalance()
            } else {
                rebalancing_node
            };

            node.replace(balanced_node);
        }
    }

    fn rebalance(self) -> Box<Node<T>> {
        enum HighSide {
            Left,
            Right,
        }

        let higher_side = match self
            .right
            .as_ref()
            .map_or(0, |n| n.height)
            .cmp(self.left.as_ref().map_or(&0, |n| &n.height))
        {
            Ordering::Less => HighSide::Left,
            Ordering::Equal => panic!("No high side"),
            Ordering::Greater => HighSide::Right,
        };

        let new_root = match higher_side {
            HighSide::Left => {
                if self.left.as_ref().unwrap().balance_factor() <= 0 {
                    Node::rotate_right(self)
                } else {
                    Node::rotate_left_right(self)
                }
            }
            HighSide::Right => {
                if self.right.as_ref().unwrap().balance_factor() >= 0 {
                    Node::rotate_left(self)
                } else {
                    Node::rotate_right_left(self)
                }
            }
        };

        Box::new(new_root)
    }

    fn needs_rebalance(&self) -> bool {
        matches!(self.balance_factor(), 2 | -2)
    }

    fn rotate_left(mut x: Node<T>) -> Node<T> {
        let mut z = x
            .right
            .take()
            .expect("invalid rebalancing with no sub-child");
        let left_child = z.left.take();

        x.right = left_child;
        x.update_height();

        z.left = Some(Box::new(x));
        z.update_height();

        *z
    }

    fn rotate_right(mut x: Node<T>) -> Node<T> {
        let mut z = x
            .left
            .take()
            .expect("invalid rebalancing with no sub-child");
        let right_child = z.right.take();
        x.left = right_child;
        x.update_height();

        z.update_height();
        z.right.replace(Box::new(x));

        *z
    }

    fn rotate_right_left(mut x: Node<T>) -> Node<T> {
        let mut z = x
            .right
            .take()
            .expect("invalid rebalancing with no sub-child");

        let mut y = z
            .left
            .take()
            .expect("invalid right left with no right left sub-child");

        let y_right_child = y.right.take();
        z.left = y_right_child;
        z.update_height();

        let y_left_child = y.left.take();
        x.right = y_left_child;
        x.update_height();

        y.right.replace(z);
        y.left.replace(Box::new(x));
        y.update_height();

        *y
    }

    fn rotate_left_right(mut x: Node<T>) -> Node<T> {
        let mut z = x
            .left
            .take()
            .expect("invalid rebalancing with no sub-child");

        let mut y = z
            .right
            .take()
            .expect("invalid left right with no left right sub-child");

        let y_left_child = y.left.take();
        z.right = y_left_child;
        z.update_height();

        let y_right_child = y.right.take();
        x.left = y_right_child;
        x.update_height();

        y.left.replace(z);
        y.right.replace(Box::new(x));
        y.update_height();

        *y
    }

    pub(crate) fn update_height(&mut self) {
        self.height = max(
            self.left.as_ref().map_or(0, |n| n.height),
            self.right.as_ref().map_or(0, |n| n.height),
        ) + 1;
    }

    pub(crate) fn contains<Q>(&self, element: &T) -> bool
    where
        T: Borrow<Q> + Ord,
        Q: Ord + ?Sized,
    {
        self.get(element.borrow()).is_some()
    }

    pub(crate) fn get<Q>(&self, element: &Q) -> Option<&T>
    where
        T: Borrow<Q> + Ord,
        Q: Ord + ?Sized,
    {
        match element.cmp(self.value.borrow()) {
            Ordering::Less => self.left.as_ref().and_then(|node| node.get(element)),
            Ordering::Greater => self.right.as_ref().and_then(|node| node.get(element)),
            Ordering::Equal => Some(&self.value),
        }
    }
}

impl<T: Debug + Ord> Debug for Node<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Node")
            .field("value", &self.value)
            .field("height", &self.height)
            .field("left", &self.left)
            .field("right", &self.right)
            .finish()
    }
}

impl<T: Clone + Ord> Clone for Node<T> {
    fn clone(&self) -> Self {
        Self {
            value: self.value.clone(),
            left: self.left.clone(),
            right: self.right.clone(),
            height: self.height,
        }
    }
}

#[derive(Error, Debug, Copy, Clone)]
#[error("Element already exists")]
pub struct ElementAlreadyExists;

#[derive(Error, Debug, Copy, Clone)]
#[error("Element does not exist")]
pub struct ElementDoesNotExist;
